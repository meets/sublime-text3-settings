# Sublime Text 3 パッケージセット

## はじめに
SublimeText3で自分が使っているパッケージと設定。
汎用化しているわけではないのでパスの指定などが一致しない環境もあるかも。

## インストール

    cd ~/Library/Application\ Support

    # 元のファイルは削除して良い
    rm -rf ./Sublime\ Text\ 3

    git clone https://meets@bitbucket.org/meets/sublime-text3-settings.git Sublime\ Text\ 3
